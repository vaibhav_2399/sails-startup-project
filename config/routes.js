/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

var otherRoutes = {

  "/": { view: "pages/homepage" },

  // CRUD OPERATION WITH POSTMAN API
  "POST /api/v1/user/create": { action: "user/create" },
  "PUT /api/v1/user/update": { action: "user/update" },
  "GET /api/v1/user": { action: "user/index" },
  "DELETE /api/v1/user/delete/:userId": { action: "user/delete" },

  //file upload api
  "POST /api/v1/fileupload": { action: "fileupload" },
  "GET  /api/v1/post/:id/get-photo": {
    action: "post/get-photo",
    skipAssets: false,
  },

  // test route
  "GET /email/confirm": {
    action: "common/confirm-email",
    locals: { layout: "admin/layouts/layout" },
  },
  "GET /test-job": { action: "common/test-job" },
  "GET /test-mail": { action: "common/test-mail" },
  "GET /data-seed": { action: "common/data-seed" },

  "POST /graphql": { action: "graphql/graphql" },
  // user register and login
  "GET /signup": { action: "entrance/view-signup" },
  "POST /api/v1/entrance/signup": { action: "entrance/signup" },
  "GET /login": { action: "entrance/view-login" },
  "POST /api/v1/entrance/login": { action: "entrance/login" },
  "POST /api/v1/user/login": { action: "entrance/loginapi" },

  "GET /member": { action: "clone/add-member" },
  "POST /member/create": { action: "clone/member" },

  "GET /croper": { action: "croper/add-croper" },
  "POST /croper/create": { action: "croper/croper" },
};

var adminRoutes = {

  "GET /login": {
    action: "admin/admin/auth/view-login",
    locals: { layout: "admin/layouts/layout" },
  },

  // Not Secured API
  "POST /login": {
    action: "admin/admin/auth/login",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /logout": {
    action: "admin/admin/auth/logout",
    locals: { layout: "admin/layouts/layout" },
  },

  // Secured Views and APIs
  "GET /dashboard": {
    action: "admin/dashboard/view-index",
    locals: { layout: "admin/layouts/master" },
  },

  // admin
  "GET /admins": {
    action: "admin/admin/view-index",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /create": {
    action: "admin/admin/view-create",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /edit/:adminId": {
    action: "admin/admin/view-edit",
    locals: { layout: "admin/layouts/master" },
  },

  "GET /index": { action: "admin/admin/index" }, //getting list of admin with datatable
  "POST /create": { action: "admin/admin/create" },
  "POST /update/:adminId": {
    action: "admin/admin/update",
    locals: { layout: "admin/layouts/master" },
  },
  "DELETE /:id": { action: "admin/admin/delete" },

  // user
  "GET /users": {
    action: "admin/user/view-index",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /user/create": {
    action: "admin/user/view-create",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /user/edit/:userId": {
    action: "admin/user/view-edit",
    locals: { layout: "admin/layouts/master" },
  },

  "GET /user/index": { action: "admin/user/index" }, //getting list of admin with datatable
  "POST /user/create": { action: "admin/user/create" },
  "POST /user/update/:userId": { action: "admin/user/update" },
  "DELETE /user/:id": { action: "admin/user/delete" },

  // =================================== ROLE ===========================================================

  "GET /role": {
    action: "admin/role/view-index",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /role/create": {
    action: "admin/role/view-create",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /role/edit/:id": {
    action: "admin/role/view-edit",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /role/index": { action: "admin/role/index" },
  "GET /role/datatable": { action: "admin/role/datatable" },
  "POST /role/create": { action: "admin/role/create" },
  "DELETE /role/:id": { action: "admin/role/delete" },
  "POST /role/update/:id": { action: "admin/role/update" },
  "POST /role/namecheck": { action: "admin/role/namecheck" },

  // =================================== PERMISSIONS ========================================================

  "GET /permission": {
    action: "admin/permission/view-index",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /permission/create": {
    action: "admin/permission/view-create",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /permission/edit/:id": {
    action: "admin/permission/view-edit",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /permission/index": { action: "admin/permission/index" },
  "POST /permission/create": { action: "admin/permission/create" },
  "POST /permission/update/:id": { action: "admin/permission/update" },
  "GET /permission/datatable": { action: "admin/permission/datatable" },


  // =================================== ROLE PERMISSIONS ========================================================

  "GET /role-permission": {
    action: "admin/rolepermission/view-index",
    locals: { layout: "admin/layouts/master" },
  },
  "GET /role-permission/datatable": {
    action: "admin/rolepermission/datatable",
  },
  "GET /role-permission/edit/:id": {
    action: "admin/rolepermission/view-edit",
    locals: { layout: "admin/layouts/master" },
  },
  "POST /role-permission/update/:id": { action: "admin/rolepermission/update" },
};

function genRoutes(objRoutes) {
  // console.log(objRoutes);
  var prefix = Object.keys(objRoutes);
  let newRoutes = {};
  let routes = {};
  // console.log(prefix);

  for (let i = 0; i < prefix.length; i++) {
    var paths = Object.keys(objRoutes[prefix[i]]);
    // console.log('paths =>', paths);
    paths.forEach(function (path) {
      var pathParts = path.split(" "),
        uri = pathParts.pop(),
        prefixedURI = "",
        newPath = "";

      prefixedURI = (prefix[i] ? "/" : "") + prefix[i] + uri;
      pathParts.push(prefixedURI);
      newPath = pathParts.join(" ");
      // construct the new routes
      newRoutes[newPath] = objRoutes[prefix[i]][path];
    });
  }
  routes = newRoutes;
  // console.log('routes => ', routes);
  return routes;
}

// generate route with prefix keys
var routes = genRoutes({
  "": otherRoutes,
  admin: adminRoutes,
});
module.exports.routes = routes;
