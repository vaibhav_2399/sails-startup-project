/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
	return knex.schema.createTable('permissionRoles', (t) => {
		t.increments('id').primary();
		t.datetime('createdAt').nullable();
		t.datetime('updatedAt').nullable();
        t.integer('permission').nullable();
        t.integer('role').nullable();
	});
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  return knex.schema.dropTableIfExists('permissionRoles');
};
