/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  return knex.schema.createTable("user", function(t) {
    t.increments("id").primary();

    t.string("firstName").notNullable();
    t.string("lastName").notNullable();
    t.string("email").notNullable();
    t.string("password").notNullable();
    t.string("phoneNumber");
    t.string("profileImage");
    t.string("companyName");
    t.enu("gender", ["male", "female"]);
    t.date("dob");
    t.string("timeZone");
    t.boolean("isDeleted");
    t.enu("role", ["admin","user",]);
    t.string("countryCode");
    t.integer("country").unsigned();
    t.integer("state").unsigned();
    t.integer("city").unsigned();
    t.string("verificationToken");
    t.timestamp("createdAt", { useTz: true });
    t.timestamp("updatedAt", { useTz: true });
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  return knex.schema.dropTableIfExists("user");
};
