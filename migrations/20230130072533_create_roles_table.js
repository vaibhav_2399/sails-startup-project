/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  return knex.schema.createTable('roles', (t) => {
		t.increments('id').primary();
		t.datetime('createdAt').nullable();
		t.datetime('updatedAt').nullable();
        t.string('name').nullable();
        t.string('displayName').nullable();
        t.string('description').nullable();
        t.integer('permissions').nullable();
        t.integer('user').nullable();
	});
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  return knex.schema.dropTableIfExists('roles');
};
