/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable("admins", function(t) {
        t.increments("id").primary();

        t.string("name").notNullable();
        t.string("email").notNullable();
        t.string("password").notNullable();

        t.timestamp("createdAt", { useTz: true });
        t.timestamp("updatedAt", { useTz: true });
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTableIfExists("admins");
};
