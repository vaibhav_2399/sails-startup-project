/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
  return knex.schema.createTable("post", function(t) {
    t.increments("id").primary();

    t.string("imageUploadFd").notNullable();
    t.string("imageUploadMime").notNullable();

    t.timestamp("createdAt", { useTz: true });
    t.timestamp("updatedAt", { useTz: true });
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
  return knex.schema.dropTableIfExists("post");
};
