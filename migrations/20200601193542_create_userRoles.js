exports.up = function (knex) {
	return knex.schema.createTable('userRoles', (t) => {
		t.increments('id').primary();
		t.datetime('createdAt').nullable();
		t.datetime('updatedAt').nullable();
        t.integer('user').nullable();
        t.integer('role').nullable();
	});
};

exports.down = function (knex) {
	return knex.schema.dropTableIfExists('userRoles');
};
