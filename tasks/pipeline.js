
var cssFilesToInject = [

  'dependencies/**/*.css',

  'styles/**/*.css'
];

//Admin css
var cssFilesToInjectAdmin = [

  "admin/**/*.css",
];


var jsFilesToInject = [

  'dependencies/sails.io.js',
  'dependencies/**/*.js',
  'js/**/*.js'
];

//Admin js
var jsFilesToInjectAdmin = [


  "admin/js/jquery-3.1.1.min.js",
  "admin/js/jquery.metisMenu.js",
  "admin/js/jquery.slimscroll.js",
  "admin/js/pace.min.js",
  "admin/js/popper.min.js",
  "admin/js/bootstrap.js",
  "admin/js/inspinia.js",
  "admin/js/axios.js",
  "admin/plugins/validate/jquery.validate.min.js",
  "admin/plugins/toastr/toastr.min.js",
  "admin/plugins/sweetalert2/sweetalert2.min.js",
  "admin/plugins/iCheck/icheck.min.js",
  "admin/plugins/select2/select2.full.min.js",

];


var templateFilesToInject = [
  'templates/**/*.html'
];
var templateFilesToInjectAdmin = ["templates/**/*.html"];



var tmpPath = '.tmp/public/';

module.exports.cssFilesToInject = cssFilesToInject.map((cssPath)=>{
  // If we're ignoring the file, make sure the ! is at the beginning of the path
  if (cssPath[0] === '!') {
    return require('path').join('!' + tmpPath, cssPath.substr(1));
  }
  return require('path').join(tmpPath, cssPath);
});
module.exports.jsFilesToInject = jsFilesToInject.map((jsPath)=>{
  // If we're ignoring the file, make sure the ! is at the beginning of the path
  if (jsPath[0] === '!') {
    return require('path').join('!' + tmpPath, jsPath.substr(1));
  }
  return require('path').join(tmpPath, jsPath);
});
module.exports.templateFilesToInject = templateFilesToInject.map((tplPath)=>{
  // If we're ignoring the file, make sure the ! is at the beginning of the path
  if (tplPath[0] === '!') {
    return require('path').join('!assets/', tplPath.substr(1));
  }
  return require('path').join('assets/', tplPath);
});

//FOR ADMIN
module.exports.cssFilesToInjectAdmin = cssFilesToInjectAdmin.map(cssPath => {
  // If we're ignoring the file, make sure the ! is at the beginning of the path
  if (cssPath[0] === "!") {
    return require("path").join("!" + tmpPath, cssPath.substr(1));
  }
  return require("path").join(tmpPath, cssPath);
});
module.exports.jsFilesToInjectAdmin = jsFilesToInjectAdmin.map(jsPath => {
  // If we're ignoring the file, make sure the ! is at the beginning of the path
  if (jsPath[0] === "!") {
    return require("path").join("!" + tmpPath, jsPath.substr(1));
  }
  return require("path").join(tmpPath, jsPath);
});
module.exports.templateFilesToInjectAdmin = templateFilesToInjectAdmin.map(tplPath => {
  // If we're ignoring the file, make sure the ! is at the beginning of the path
  if (tplPath[0] === "!") {
    return require("path").join("!assets/", tplPath.substr(1));
  }
  return require("path").join("assets/", tplPath);
});
