/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  const moment = require("moment");
  const currentDateTime = moment().utc().format("YY-MM-DD HH:mm:ss");
  await knex('admins').del()
  await knex('admins').insert([
    {
      id: 1,
      name: 'Super Admin',
      email: 'admin@admin.com',
      password: '$2a$10$4Nc8PvwLgiJWcE4mcibv9uIpdlz7yYbu2wkpQXkdyhFbjzlwZOYBS',
      createdAt: currentDateTime,
      updatedAt: currentDateTime,
    },
  ]);
};
