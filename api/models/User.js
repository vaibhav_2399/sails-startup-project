/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  fetchRecordsOnUpdate: true,
  customToJSON: function () {
    return _.omit(this, ['password'])
  },
  attributes: {

    firstName: {
      type: 'string',
      required: true,
    },

    lastName: {
      type: 'string',
      required: true,
    },

    email: {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true,
    },

    password: {
      type: 'string',
      required: true,
      protect: true,
    },
    role: {
			collection: "Role",
			via: "user",
			through: "UserRole",
		},
    verificationToken: {
      type: 'string',
    },


    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },
  customToJSON: function () {
    return _.omit(this, ['password', 'verificationToken'])
  },
  beforeCreate: async function (valuesToSet, proceed) {
    // Hash password
    // console.log('beforeCreate', valuesToSet);
    await sails.helpers.passwords
      .hashPassword(valuesToSet.password)
      .exec((err, hashedPassword) => {
        if (err) {
          return proceed(err);
        }
        valuesToSet.password = hashedPassword;
        valuesToSet.verificationToken =
          sails.helpers.strings.random("url-friendly");
        return proceed();
      });
  },
  afterCreate: async function (valuesToSet, proceed) {
    console.log('afterCreate', valuesToSet, sails.helpers.strings.random('url-friendly'));
    if (sails.config.custom.verifyEmailAddresses) {
      await sails.helpers.sendEmail.with({
        to: valuesToSet.email,
        subject: 'Please confirm your account',
        template: 'email-verify-account',
        layout: 'layout-email',
        typeOfSend: 'now', // 'now', 'queue', 'preview'
        templateData: {
          id: valuesToSet.id,
          fullName: valuesToSet.firstName + ' ' + valuesToSet.lastName,
          token: valuesToSet.verificationToken
        }
      });
    } else {
      sails.log.info('Skipping new account email verification... (since `verifyEmailAddresses` is disabled)');
    }
    return proceed();
  },
};

