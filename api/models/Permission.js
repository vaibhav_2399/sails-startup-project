module.exports = {
	tableName: "permissions",
	fetchRecordsOnUpdate: true,
	fetchRecordsOnDestroy: true,
	attributes: {
		name: {
			type: "string",
			required: true,
		},
		displayName: {
			type: "string",
			required: true,
		},
		moduleName: {
			type: "string",
			allowNull: true,
		},
		description: {
			type: "string",
			required: true,
		},
		roles: {
			collection: "role", // model name
			via: "permission", // permission role attributes -> permission
			through: "PermissionRole", // table name of pivot table
		},
	},
};
