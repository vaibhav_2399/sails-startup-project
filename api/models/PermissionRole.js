module.exports = {
	tableName: "permissionRoles",
	fetchRecordsOnUpdate: true,
	fetchRecordsOnDestroy: true,
	attributes: {
		permission: {
			model: "Permission",
		},
		role: {
			model: "Role",
		},
	},
};
