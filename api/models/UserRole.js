module.exports = {
	tableName: "userRoles",
	fetchRecordsOnUpdate: true,
	fetchRecordsOnDestroy: true,
	attributes: {
		user: {
			model: "User",
		},
		role: {
			model: "Role",
		},
	},
};
