/**
 * Admin.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'admins',
  attributes: {
    'name': {
      type: 'string',
      required: true,
    },

    'email': {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true,
    },
    'password': {
      type: 'string',
      required: true,
      protect: true
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },
  customToJSON: function () {
    return _.omit(this, ["password"]);
  },
  beforeCreate: async function (valuesToSet, proceed) {
    // Hash password
    // console.log('beforeCreate', valuesToSet);
    await sails.helpers.passwords
      .hashPassword(valuesToSet.password)
      .exec((err, hashedPassword) => {
        if (err) {
          return proceed(err);
        }
        valuesToSet.password = hashedPassword;
        // valuesToSet.verificationToken =
        //   sails.helpers.strings.random("url-friendly");
        return proceed();
      });
  },

};

