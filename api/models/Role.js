module.exports = {
	tableName: "roles",
	fetchRecordsOnUpdate: true,
	fetchRecordsOnDestroy: true,
	attributes: {
		name: {
			type: "string",
			required: true,
			unique: true,
		},
		displayName: {
			type: "string",
			required: true,
		},
		description: {
			type: "string",
			required: true,
		},
		permissions: {
			collection: "permission",
			via: "role",
			through: "PermissionRole",
		},
		user: {
			collection: "User",
			via: "role",
			through: "UserRole",
		},
	},
};
