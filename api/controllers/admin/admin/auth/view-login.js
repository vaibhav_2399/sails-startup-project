module.exports = {


    friendlyName: 'View form',


    description: 'Display "Form" page.',


    exits: {

      success: {
        viewTemplatePath: 'admin/login',
      }

    },


    fn: async function () {

      // Respond with view.
      return {};

    }


  };