module.exports = {
  friendlyName: "View create",

  description: 'Display "Create" page.',

  exits: {
    success: {
      viewTemplatePath: "admin/permission/create",
    },
  },

  fn: async function(inputs, exits) {
    let permissionArrList = sails.config.permissions;
    let arrPermissions = [];
    for (var key in permissionArrList) {
      var arrSingleModulePermission = permissionArrList[key];
      arrPermissions = arrPermissions.concat(arrSingleModulePermission);
    }

    let nameList = _.map(arrPermissions, "name");
    let permissionResult = await Permission.find({
      name: { in: nameList },
    });
    //  if (Object.keys(permissionResult).length > 0) {
    //   let permission = _.differenceWith(arrPermissions, permissionResult, function(permission1, permission2) {
    //     return permission1["name"] === permission2["name"];
    //   });
    //   return this.res.json(permission);
    // }
    // return this.res.json(permissionResult);

  //  var data1 = JSON.parse(arrPermissions);

    // let result = await sails.helpers.datatable.with({
    //   model: Permission,
    //   options: this.req.query,
    // });
    // return this.res.json(permissionArrList);
    return exits.success({ permissionRecord: arrPermissions });
  },
};
