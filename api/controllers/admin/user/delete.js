module.exports = {


  friendlyName: 'Delete',


  description: 'Delete User.',


  inputs: {

  },


  exits: {
    success: {
      statusCode: 200,
      description: 'user destroy success'
    },
    invalid: {
      statusCode: 502,
      responseType: 'badRequest',
      description: 'user destroy error'
    },
    redirect: {
      responseType: 'redirect'
    }
  },


  fn: async function (inputs, exits) {

    var userRecord = await User.destroy({
      id: inputs.userId
    }).fetch();

    if((userRecord.length === 0)){
      return exits.invalid({
        message: 'No record found',
      });
    }
    return exits.success({
      message: 'User has been deleted successfully.',
    });
  }


};
