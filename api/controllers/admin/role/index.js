module.exports = {
  friendlyName: "Index",

  description: "Index role.",

  inputs: {},

  exits: {},

  fn: async function(inputs, exits) {
    let result = await sails.helpers.datatable.with({
      model: Role,
      options: this.req.query,
    });

    console.log("role",result);

    return exits.success(result);
  },
};
