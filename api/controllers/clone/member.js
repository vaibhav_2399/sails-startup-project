module.exports = {
  friendlyName: "Member",

  description: "Member clone.",

  inputs: {
    firstName: {
      type: "string",
      required: false,
    },
    lastName: {
      type: "string",
      required: false,
    },
    email: {
      required: false,
      unique: true,
      type: "string",
      isEmail: true,
    },
    password: {
      required: false,
      type: "string",
      maxLength: 15,
      minLength: 6,
    },
  },

  exits: {
    success: {
      message: "User create successfully",
    },
    invalid: {
      statusCode: 409,
      description: "Name and City is required.",
    },
    redirect: {
      responseType: "redirect",
    },
  },

  fn: async function (req, res) {
    var members = this.req.body;
    var firstName = {};
    var lastName = {};
    var email = {};
    var password = {};
    for (const key in members) {
      const element = members[key];
      if (key === "firstName[]") {
        firstName = element;
      } else if (key === "lastName[]") {
        lastName = element;
      } else if (key === "email[]") {
        email = element;
      } else if (key === "password[]") {
        password = element;
      }
    }
    try {
      if (typeof firstName === "string") {
        var createdmemberlist = await User.create({
          firstName: firstName,
          lastName: lastName,
          email: email,
          password: password,
        }).fetch();
      } else {
        var memberlist = firstName.map((firstName, index) => {
          return {
            firstName: firstName,
            lastName: lastName[index],
            email: email[index],
            password: password[index],
          };
        });
        var createdmemberlist = await User.createEach(memberlist).fetch();
      }
      // Add the memberlist to the database using createEach()

      if (!createdmemberlist) return;

      this.req.session.flash = {
        type: "success",
        message: "Member create successfully",
      };
      return this.res.redirect("/");
    } catch (error) {
      // console.log('error', error)
      return res.serverError(error);
    }
  },
};
