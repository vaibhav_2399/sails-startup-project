module.exports = {


  friendlyName: 'View form',


  description: 'Display "Form" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/clone/add-members'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
