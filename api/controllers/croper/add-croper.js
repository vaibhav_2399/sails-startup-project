module.exports = {


  friendlyName: 'View form',


  description: 'Display "Form" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/croper/croper'
    }

  },


  fn: async function () {

    // Respond with view.
    return {};

  }


};
