module.exports = {
  friendlyName: "croper",

  description: "croper.",

  inputs: {
    imgData: {
      type: "ref",
      required: false,
    },

  },

  exits: {
    success: {
      message: "User create successfully",
    },
    invalid: {
      statusCode: 409,
      description: "Name and City is required.",
    },
    redirect: {
      responseType: "redirect",
    },
  },

  fn: async function (inputs, exits) {
    var fs = require('fs');
    fs.mkdir('.tmp/uploads', {recursive: true}, err => {})

    var base64Data = this.req.body.imgData.split(',')[1];
    fs.writeFile(".tmp/uploads/"+Date.now()+".png", base64Data, 'base64', function(err){
      if (err) {
        return exits.invalid({
          message: 'Something is wrong please try again',
        });
      } else {
        // res.sendFile('image_dev.png', { root: __dirname });
        return exits.success({
          message: 'Image Uploaded successfully',
        });
      }
    });

  },
};
