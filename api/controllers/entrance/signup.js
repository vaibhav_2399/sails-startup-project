module.exports = {


  friendlyName: 'Signup',


  description: 'Signup entrance.',


  inputs: {
    email: {
      required: true,
      type: 'string',
      isEmail: true,
      description: 'The email address for the new account, e.g. m@example.com.',
      extendedDescription: 'Must be a valid email address.',
    },

    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'passwordlol',
      description: 'The unencrypted password to use for the new account.'
    },

    firstName: {
      type: 'string',
      required: true
    },
    lastName: {
      type: 'string',
      required: true
    },
  },


  exits: {
    success: {
      description: 'New user account was created successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided fullName, password and/or email address are invalid.',
      extendedDescription: 'If this request was sent from a graphical user interface, the request '+
      'parameters should have been validated/coerced _before_ they were sent.'
    },

    emailAlreadyInUse: {
      statusCode: 409,
      description: 'The provided email address is already in use.',
    },
  },


  fn: async function (inputs, exits) {
    var newEmailAddress = inputs.email.toLowerCase();

    var userRecord = await User.create({
      email: newEmailAddress,
      password: inputs.password,
      firstName: inputs.firstName,
      lastName: inputs.lastName,
    })
    .intercept('E_UNIQUE', (err) => {
      this.req.session.flash = {
        type: "error",
        message: "User already exsist",
      };
      return this.res.redirect("/");
    })
    .fetch();
    this.req.session.userId = userRecord.id;

    if (!userRecord) return;

    this.req.session.flash = {
      type: "success",
      message: "User create successfully",
    };
    return this.res.redirect("/");
  }


};
