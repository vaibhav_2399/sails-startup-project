module.exports = {


  friendlyName: 'Login',


  description: 'Login entrance.',


  inputs: {
    email: {
      type: 'string',
      required: true
    },

    password: {
      type: 'string',
      required: true
    },

    rememberMe: {
      description: 'Whether to extend the lifetime of the user\'s session.',
      type: 'boolean'
    }
  },


  exits: {
    success: {
      description: 'The requesting user agent has been successfully logged in.',
    },

    badCombo: {
      description: `The provided email and password combination does not match any user in the database.`,
      responseType: 'unauthorized'
    }
  },


  fn: async function (inputs, exits) {

    console.log('inputs', inputs);
    var userRecord = await User.findOne({
      email: inputs.email.toLowerCase(),
    });

    // If there was no matching user, respond thru the "badCombo" exit.
    if(!userRecord) {
      throw 'badCombo';
    }

    // If the password doesn't match, then also exit thru "badCombo".
    // await sails.helpers.passwords.checkPassword(inputs.password, userRecord.password)
    // .intercept('incorrect', 'badCombo');

    // If "Remember Me" was enabled, then keep the session alive for
    // a longer amount of time.  (This causes an updated "Set Cookie"
    // response header to be sent as the result of this request -- thus
    // we must be dealing with a traditional HTTP request in order for
    // this to work.)

    // Modify the active session instance.
    // (This will be persisted when the response is sent.)
    this.req.session.user = userRecord;
    this.req.session.userId = userRecord.id;

    if (!userRecord) return;

    this.req.session.flash = {
      type: "success",
      message: "User login successfully",
    };
    return this.res.redirect("/");

  }


};
