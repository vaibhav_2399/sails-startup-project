module.exports = {


  friendlyName: 'Get photo',


  description: '',


  inputs: {
    id: {
      description: 'The id of the item whose photo we\'re downloading.',
      type: 'number',
      required: true
    }
  },


  exits: {
    success: {
      outputDescription: 'The streaming bytes of the specified thing\'s photo.',
      outputType: 'ref'
    },

    forbidden: { responseType: 'forbidden' },

    notFound: { responseType: 'notFound' }
  },


  fn: async function ({id}) {
    var photo = await Post.findOne({id});
    if (!photo) { throw 'notFound'; }

    this.res.type(photo.imageUploadMime);
    
    var downloading = await sails.startDownload(photo.imageUploadFd);

    return downloading;

  }


};
