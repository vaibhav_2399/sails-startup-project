module.exports = {
  friendlyName: "Create",

  description: "Create user.",

  inputs: {
    photo: {
      required: false,
      type: "ref",
    },
  },

  exits: {
    success: {
      message: "User create successfully",
    },
    invalid: {
      statusCode: 409,
      description: "Name and City is required.",
    },
    redirect: {
      responseType: "redirect",
    },
  },

  fn: async function (inputs, exits) {
    this.req.file("photo").upload(
      {
        dirname: require("path").resolve(sails.config.appPath, "assets/images"),
      },
      async function (err, uploadedFiles) {
        if (err) return res.serverError(err);
        var myString = uploadedFiles[0].fd;
        const imageupload = myString.split("assets/");
        img_upload_data = imageupload[1];

        var userRecord = await Post.create({
          imageUploadFd	: img_upload_data,
          imageUploadMime	:img_upload_data,
        })
          .intercept("E_UNIQUE", (err) => {
            this.req.session.flash = {
              type: "error",
              message: "User already exsist",
            };
            return this.res.redirect("/home");
          })
          .fetch();

        if (!userRecord) return;

        if (err) {
          console.error(err);
          res.status(500).send('Internal server error');
        } else {
          // res.sendFile('image_dev.png', { root: __dirname });
          return exits.success({
            data: userRecord,
            message: 'Post uploaded successfully.',
          });
        }
      }
    );
  },
};
