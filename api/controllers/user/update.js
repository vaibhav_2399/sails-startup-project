module.exports = {


  friendlyName: 'Update',


  description: 'Update user.',


  inputs: {
    userId: {
      type: 'string',
      required: true
    },
    firstName: {
      type: 'string',
      required: true
    },
    lastName: {
      type: 'string',
      required: true
    },
    email: {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true
    },
    password: {
      type: 'string',
      required: true,
      minLength:6
    }
  },


  exits: {
    success: {
      statusCode: 200,
      description: 'user create success'
    },
    invalid: {
      statusCode: 502,
      responseType: 'badRequest',
      description: 'user destroy error'
    },
  },


  fn: async function (inputs, exits) {

    var updatedUser = await User.update({
      id: inputs.userId
    }).set({
        firstName: inputs.firstName,
        lastName: inputs.lastName,
        email: inputs.email,
        password:inputs.password
    }).fetch();
    if((updatedUser.length === 0)){
      return exits.invalid({
        message: 'User not exists',
      });
    }
    return exits.success({
      message: 'User has been updated successfully.',
    });
  }


};
