module.exports = {


  friendlyName: 'Create',


  description: 'Create user.',


  inputs: {
    firstName: {
      type: 'string',
      required: true
    },
    lastName: {
      type: 'string',
      required: true
    },
    email: {
      type: 'string',
      required: true,
      unique: true,
      isEmail: true
    },
    password: {
      type: 'string',
      required: true,
      minLength:6
    }

  },


  exits: {
    success: {
      statusCode: 200,
      description: 'user create success'
    },
    invalid: {
      statusCode: 502,
      responseType: 'badRequest',
      description: 'user create error'
    },
    emailAlreadyInUse: {
      statusCode: 409,
      description: 'The provided email address is already in use.',
    },

  },



  fn: async function (inputs, exits) {
    var userRecord = await User.create({
      firstName: inputs.firstName,
       lastName: inputs.lastName,
       email: inputs.email,
       password: inputs.password
    }).intercept('E_UNIQUE', (err)=> {
      return exits.emailAlreadyInUse({
        message: 'The provided email address is exists.'
      })
    }).fetch();
    if(!userRecord){
      return exits.invalid({
        message: 'Some thing went wrong. Please try again',
      });
    }
    return exits.success({
      message: 'User has been created successfully.',
      data: userRecord
    });

  }


};
