/* eslint-disable eqeqeq */
module.exports = {
  friendlyName: "Create account",

  description: "",

  inputs: {
    field: {
      type: "ref",
    },
  },

  exits: {
    success: {},
    invalid: {},
    badRequest: {},
  },

  fn: async function (inputs, exits) {
    var name = inputs.field;

    if (name == "" || name == undefined) {
      return exits.success(false);
    } else {
      return exits.success(true);
    }
  },
};
